# TDD Framework

# Below are some of the features of this test framework:

* Maven based framework
* Extent Report for reporting
* Report Generation (cucumber-reporting)
* Helper class to handle web component such as (click,sendkeys etc)
* POM with page factory (Page Object Model is a design pattern to create Object Repository for web UI elements. Page Factory is an inbuilt Page Object Model concept for Selenium WebDriver but it is very optimized.)
* Different browser support (chrome,firefox)


## Documentation

* [Installation](https://github.com/selenium-cucumber/selenium-cucumber-java/blob/master/doc/installation.md)
* [Install IntelliJ](https://www.jetbrains.com/idea/download/#section=linux) for easy use of lambda expression
* Configure and Install Following Intellij Plugins
	* File -> Setting -> Plugins ->
	* Maven Integration 


# Setting up and running tests

* Open project command line and run the following command
	* mvn clean compile test 

* Note: All code currently under src>test>java>com>turtlemint>basesuite>TurtleMintTest.java, We can optimize and refactor.
As per time limitation completed the code.

* Report: Navigate to the reports folder and you can view entire run details.
 
* Excel for parameter: src>test>resources>Excel