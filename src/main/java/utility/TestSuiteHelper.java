package main.java.utility;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import main.java.constants.Constant;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import test.java.com.turtlemint.basesuite.BaseUtil;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Random;

public class TestSuiteHelper extends BaseUtil {

    public static String takeScreenShot() {

        Date dateObj = new Date();
        String fileName = dateObj.toString().replace(":", "_").replace(" ", "_") + Thread.currentThread().getStackTrace()[2].getMethodName() + ".png";
        String filePath = Constant.TakeScreenShotConstant.SCREENSHOTPATH + fileName;
        System.out.println("--------" + filePath);

        Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100))
                .takeScreenshot(driver);

        //File scrFile = ((org.openqa.selenium.TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            //FileUtils.copyFile(scrFile, new File(filePath));
            ImageIO.write(screenshot.getImage(), "PNG",
                    new File(filePath));
        } catch (IOException e) {
            extentTest.log(Status.FAIL, "Exception :" + e);
        }

        return filePath;
    }

    public static void takeScreenShotHelper(ExtentTest extentTest) {
        try {
            String filePath = takeScreenShot();
            extentTest.addScreenCaptureFromPath(filePath);
        } catch (IOException ex) {
            extentTest.fail("Exception:->" + ex);
        }

    }

    public static int generateRandomDigits(int n) {
        int m = (int) Math.pow(10, n - 1);
        return m + new Random().nextInt(9 * m);
    }

    public static void verifyLinks(String url) {
        HttpURLConnection huc = null;
        int respCode = 200;
        try {
            huc = (HttpURLConnection) (new URL(url).openConnection());

            huc.setRequestMethod("HEAD");

            huc.connect();

            respCode = huc.getResponseCode();

            if (respCode >= 400) {
                System.out.println(url + " is a broken link");
            } else {
                System.out.println(url + " is a valid link");
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
