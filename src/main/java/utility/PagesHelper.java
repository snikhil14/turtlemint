package main.java.utility;

import main.java.constants.Constant;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import test.java.com.turtlemint.basesuite.BaseUtil;

import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;

public class PagesHelper extends BaseUtil {

    public static void clearText(WebElement element) {
        waitExplicit(element, Constant.PagesHelperConstants.ELEMENTTOBECLICKABLE, Constant.PagesHelperConstants.TIME);
        element.clear();
    }

    //get text.
    public static String getText(WebElement element) {
        waitExplicit(element, Constant.PagesHelperConstants.ELEMENTTOBECLICKABLE, Constant.PagesHelperConstants.TIME);
        String value = element.getText().toLowerCase();
        return value;
    }

    //click element.
    public static void click(WebElement element) {
        waitExplicit(element, Constant.PagesHelperConstants.ELEMENTTOBECLICKABLE, Constant.PagesHelperConstants.TIME);
        element.click();
    }

    public static void sendKeys(WebElement element, String text) {
        waitExplicit(element, Constant.PagesHelperConstants.ELEMENTTOBECLICKABLE, Constant.PagesHelperConstants.TIME);
        element.sendKeys(text);
    }

    //Wait explicit for single element the element to load.
    public static void waitExplicit(WebElement element, String type, long waittime) {
        driver.manage().timeouts().implicitlyWait(0, SECONDS);
        System.out.println("Inside WaitExplicit " + element);
        WebDriverWait wait = new WebDriverWait(driver, waittime);
        String CaseType = type;
        switch (CaseType.toLowerCase()) {
            case Constant.PagesHelperConstants.VISIBILITYOF:
                wait.until(ExpectedConditions.visibilityOf(element));
                break;
            case Constant.PagesHelperConstants.ELEMENTTOBECLICKABLE:
                wait.until(ExpectedConditions.elementToBeClickable(element));
                break;
            case Constant.PagesHelperConstants.ELEMENTTOBESELECTED:
                wait.until(ExpectedConditions.elementToBeSelected(element));
                break;
            case Constant.PagesHelperConstants.INVISIBILITYOF:
                wait.until(ExpectedConditions.invisibilityOf(element));
                break;

            default:
                System.out.println("Method Name: waitexplicit-->Wrong Method Passed");
                break;
        }
    }

    //Wait explicit for more than one element.
    public static void waitExplicitElements(List<WebElement> elements, String type, long waittime) {
        driver.manage().timeouts().implicitlyWait(0, SECONDS);
        System.out.println("Inside waitExplicitElements ");
        WebDriverWait wait = new WebDriverWait(driver, waittime);
        String CaseType = type;
        switch (CaseType.toLowerCase()) {

            case Constant.PagesHelperConstants.VISIBILITYOFALLELEMENTS:
                wait.until(ExpectedConditions.visibilityOfAllElements(elements));
                break;

            default:
                System.out.println("Method Name: waitExplicitElements-->Wrong Method Passed");
                break;
        }
    }

    public static boolean isDisplayed(WebElement element) {
        boolean flag = element.isDisplayed();
        if (flag) {
            isDisplayed(element);
        }
        return flag;
    }

    //Select element.
    public static void selectElement(WebElement element, int value) {
        waitExplicit(element, Constant.PagesHelperConstants.ELEMENTTOBECLICKABLE, Constant.PagesHelperConstants.TIME);
        Select selectObj = new Select(element);
        selectObj.selectByValue(String.valueOf(value));
        /*int index = 0;
        List<WebElement> options = selectObj.getOptions();
        for (WebElement option : options) {
            if (option.getText().equalsIgnoreCase(type))
                break;
            index++;
        }
        selectObj.selectByIndex(index);*/
    }
}
