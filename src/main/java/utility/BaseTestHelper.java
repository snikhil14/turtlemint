package main.java.utility;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import main.java.constants.Constant;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class BaseTestHelper {

    public static HashMap<String, String> getTestSuiteData() throws IOException {
        HashMap<String, String> rowColumnValue = new HashMap<>();
        FileInputStream fis = new FileInputStream(new File(Constant.EXCEL_PATH));
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet sheet = wb.getSheetAt(0);     //creating a Sheet object to retrieve object
        Iterator<Row> itr = sheet.iterator();
        while (itr.hasNext()) {
            Row row = itr.next();
            row.getCell(0).setCellType(CellType.STRING);
            row.getCell(1).setCellType(CellType.STRING);
            String keyValue = row.getCell(0).getStringCellValue();
            String pairValue = row.getCell(1).getStringCellValue();
            rowColumnValue.put(keyValue, pairValue);
        }
        return rowColumnValue;
    }

    public static ExtentTest addTestName(ExtentReports extentReport, ExtentTest extentTest) {
        extentTest = extentReport.createTest(Thread.currentThread().getStackTrace()[2].getMethodName());
        return extentTest;
    }
}
