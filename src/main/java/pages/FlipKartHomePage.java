package main.java.pages;

import main.java.constants.Constant;
import main.java.utility.PagesHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.com.turtlemint.basesuite.BaseUtil;

import java.util.List;

public class FlipKartHomePage extends BaseUtil {

    public FlipKartHomePage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[contains(text(),'✕')]")
    WebElement closeBtn;

    @FindBy(xpath = "//input[@placeholder='Search for products, brands and more']")
    WebElement searchForProducts;

    @FindBy(css = "[type='submit']")
    WebElement submitBtn;

    @FindBy(xpath = "//*[text()='Relevance']")
    WebElement relevanceFilter;

    public void clickOnCloseBtn() {
        PagesHelper.click(closeBtn);
    }

    public void setSearchForProducts(String products) {
        PagesHelper.sendKeys(searchForProducts, products);
    }

    public void clickOnSearchBtn() {
        PagesHelper.click(submitBtn);
        PagesHelper.waitExplicit(relevanceFilter, Constant.PagesHelperConstants.ELEMENTTOBECLICKABLE, Constant.PagesHelperConstants.TIME);
    }

    public List<WebElement> getLinks(){
        List<WebElement> links = driver.findElements(By.tagName("a"));
        System.out.println("No of links are "+ links.size());
        return links;
    }
}
