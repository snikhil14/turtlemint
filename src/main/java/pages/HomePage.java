package main.java.pages;

import main.java.utility.PagesHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.com.turtlemint.basesuite.BaseUtil;

public class HomePage extends BaseUtil {

    public HomePage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(linkText = "Life")
    WebElement lifeInsurance;

    public void clickOnLifeInsurance() {
        PagesHelper.click(lifeInsurance);
    }
}
