package main.java.pages;

import main.java.utility.PagesHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.com.turtlemint.basesuite.BaseUtil;

public class LifeProfile extends BaseUtil {

    public LifeProfile() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='pageLoader']//*[local-name()='svg']")
    WebElement loadingIcon;

    @FindBy(css = "input[placeholder='Enter Date']")
    WebElement dateOfBirthTxtBox;

    @FindBy(xpath = "//select[1]")
    WebElement selectYear;

    @FindBy(css = "#Radio-true span")
    WebElement smokeYes;

    @FindBy(css = "#Radio-false span")
    WebElement smokeNo;

    @FindBy(css = "[name=maxIncome] div svg")
    WebElement approxAnnualIncome;

    @FindBy(xpath = "//span[normalize-space()='Next']")
    WebElement nextBtn;

    @FindBy(css = "[name=coverAmount] div svg")
    WebElement sumAssuredAmount;

    @FindBy(css = "[name='customerName']")
    WebElement nameBox;

    @FindBy(css = "[name='paidUserMobile']")
    WebElement mobileBox;

    @FindBy(css = "[name='paidUserEmail']")
    WebElement emailBox;

    @FindBy(xpath = "//*[text()='OK, GOT IT!']")
    WebElement okGotItBtn;

    public void setGender(String genderType) {
        String gender = "//label[normalize-space()='gender']/span".replace("gender", genderType);
        PagesHelper.isDisplayed(loadingIcon);
        PagesHelper.click(driver.findElement(By.xpath(gender)));
    }

    public void setDateOfBirth(int year, int date) {
        String day = "//*[text()='date']".replace("date", String.valueOf(date));
        PagesHelper.click(dateOfBirthTxtBox);
        PagesHelper.selectElement(selectYear, year);
        PagesHelper.click(driver.findElement(By.xpath(day)));
    }

    public void setSmokeType(String smokeType) {
        if (smokeType.equalsIgnoreCase("yes"))
            PagesHelper.click(smokeYes);
        else
            PagesHelper.click(smokeNo);
    }

    public void setApproxAnnualIncome(String annualIncome) {
        String annualValue = "//*[text()='annualIncome']".replace("annualIncome", annualIncome);
        PagesHelper.click(approxAnnualIncome);
        PagesHelper.click(driver.findElement(By.xpath(annualValue)));
    }

    public void clickOnNextBtn() {
        PagesHelper.click(nextBtn);
    }

    public void waitForLoadingIcon() {
        PagesHelper.isDisplayed(loadingIcon);
    }

    public void setSumAssuredAmount(String assuredAmount) {
        String sumAmount = "//*[text()='assuredAmount']".replace("assuredAmount", assuredAmount);
        PagesHelper.click(sumAssuredAmount);
        PagesHelper.click(driver.findElement(By.xpath(sumAmount)));
    }

    public void setName(String name) {
        PagesHelper.sendKeys(nameBox, name);
    }

    public void setMobileNumber(String mobileNumber) {
        PagesHelper.sendKeys(mobileBox, mobileNumber);
    }

    public void setEmail(String email) {
        PagesHelper.sendKeys(emailBox, email);
    }

    public void clickOnOkGotItBtn() {
        PagesHelper.click(okGotItBtn);
    }

    public void clickOnInsurerDetails(String insurer) {
        String insurerValue = "//button[contains(@data-auto, 'insurer')]".replace("insurer", insurer);
        PagesHelper.click(driver.findElement(By.xpath(insurerValue)));
    }

}

