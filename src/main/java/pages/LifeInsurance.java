package main.java.pages;

import main.java.utility.PagesHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.com.turtlemint.basesuite.BaseUtil;

public class LifeInsurance extends BaseUtil {

    public LifeInsurance() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//h3[normalize-space()='Term Life Plans']")
    WebElement termLifePlans;

    public void clickOnTermLifePlans() {
        PagesHelper.click(termLifePlans);
    }
}
