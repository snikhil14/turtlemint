package main.java.constants;

public class Constant {

    public static final String PROJECTPATH = System.getProperty("user.dir");
    public static final String PROJECTOS = System.getProperty("os.name");

    public static final String EXCEL_PATH = PROJECTPATH + java.io.File.separatorChar + "src"
            + java.io.File.separatorChar + "test" + java.io.File.separatorChar + "resources"
            + java.io.File.separatorChar + "Excel" + java.io.File.separatorChar + "turtlemint.xlsx";

    public static class BaseTestConstant {
        public static final String REPORTSFOLDER = Constant.PROJECTPATH + java.io.File.separatorChar + "Reports";
        public static final String TESTCASENAME = "turtlemint-ui-test";
        public static final String URL = "https://turtlemint.com";
        public static final String FLIPKARTURL = "https://flipkart.com";
    }

    public static class ExtentManagerConstant {
        public static final String REPORTSPATH = PROJECTPATH + java.io.File.separatorChar + "Reports"
                + java.io.File.separatorChar;
    }

    public static class TakeScreenShotConstant {
        public static final String SCREENSHOTPATH = PROJECTPATH + java.io.File.separatorChar + "src" + java.io.File.separatorChar + "test" + java.io.File.separatorChar + "resources" + java.io.File.separatorChar + "ScreenShots" + java.io.File.separatorChar;
    }

    public static class PagesHelperConstants {
        public static final String VISIBILITYOF = "visibilityof";
        public static final String VISIBILITYOFALLELEMENTS = "visibilityofallelements";
        public static final String ELEMENTTOBECLICKABLE = "elementtobeclickable";
        public static final String ELEMENTTOBESELECTED = "elementtobeselected";
        public static final String INVISIBILITYOF = "invisibilityof";
        public static final int TIME = 50;
    }
}
