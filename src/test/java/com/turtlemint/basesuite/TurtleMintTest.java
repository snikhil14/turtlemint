package test.java.com.turtlemint.basesuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.github.javafaker.Faker;
import main.java.constants.Constant;
import main.java.pages.FlipKartHomePage;
import main.java.pages.HomePage;
import main.java.pages.LifeInsurance;
import main.java.pages.LifeProfile;
import main.java.utility.BaseTestHelper;
import main.java.utility.BrowserSetup;
import main.java.utility.ExtentManager;
import main.java.utility.TestSuiteHelper;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class TurtleMintTest extends BaseUtil {

    public ExtentReports extentReport;
    public ExtentTest extentTest;
    HashMap<String, String> excelValue;
    HomePage homePage;
    LifeInsurance lifeInsurance;
    LifeProfile lifeProfile;
    FlipKartHomePage flipKartHomePage;

    public TurtleMintTest() {
        extentReport = ExtentManager.getInstance();
    }

    @BeforeSuite
    public void createFolder() {
        extentTest = BaseTestHelper.addTestName(extentReport, extentTest);
        String reportPath = Constant.BaseTestConstant.REPORTSFOLDER;
        String screenShotPath = Constant.TakeScreenShotConstant.SCREENSHOTPATH;
        File reportFile = new File(reportPath);
        File screenShotFile = new File(reportPath);
        if (!reportFile.exists()) {
            extentTest.log(Status.INFO, "Create New Folder->" + Constant.BaseTestConstant.REPORTSFOLDER);
            reportFile.mkdir();
            screenShotFile.mkdir();
            extentTest.log(Status.INFO, "Folder Created Successfully->" + Constant.BaseTestConstant.REPORTSFOLDER);
        } else {
            extentTest.log(Status.INFO, "Folder Present->" + Constant.BaseTestConstant.REPORTSFOLDER);
        }
    }

    @BeforeClass
    public void setBrowserAndInitializePage() throws IOException {
        excelValue = BaseTestHelper.getTestSuiteData();
        extentTest = BaseTestHelper.addTestName(extentReport, extentTest);
        driver = BrowserSetup.openBrowser(excelValue.get("browser"));
        driver.get(Constant.BaseTestConstant.URL);
        driver.manage().window().maximize();
        extentTest.log(Status.PASS, "Browser open successfully");
        TestSuiteHelper.takeScreenShotHelper(extentTest);
        extentTest.log(Status.PASS, "Set Browser successfully.");
    }

    @Test(priority = 1)
    public void getLifeInsuranceURL() throws IOException {
        extentTest = BaseTestHelper.addTestName(extentReport, extentTest);
        Faker faker = new Faker();
        homePage = new HomePage();
        lifeInsurance = new LifeInsurance();
        lifeProfile = new LifeProfile();
        excelValue = BaseTestHelper.getTestSuiteData();
        extentTest = BaseTestHelper.addTestName(extentReport, extentTest);
        homePage.clickOnLifeInsurance();
        lifeInsurance.clickOnTermLifePlans();
        String gender = StringUtils.capitalize(excelValue.get("gender"));
        int year = Integer.parseInt(excelValue.get("year"));
        int date = Integer.parseInt(excelValue.get("date"));
        String tobacco = StringUtils.capitalize(excelValue.get("tobacco"));
        String annualIncome = excelValue.get("annualIncome");
        String sumAssured = excelValue.get("sumAssured");
        String insurer = excelValue.get("insurer");
        lifeProfile.setGender(gender);
        lifeProfile.setDateOfBirth(year, date);
        lifeProfile.setSmokeType(tobacco);
        lifeProfile.setApproxAnnualIncome(annualIncome);
        TestSuiteHelper.takeScreenShotHelper(extentTest);
        lifeProfile.clickOnNextBtn();
        lifeProfile.waitForLoadingIcon();
        lifeProfile.setSumAssuredAmount(sumAssured);
        TestSuiteHelper.takeScreenShotHelper(extentTest);
        lifeProfile.clickOnNextBtn();
        lifeProfile.waitForLoadingIcon();
        lifeProfile.setName(faker.name().firstName());
        lifeProfile.setMobileNumber("9" + String.valueOf(TestSuiteHelper.generateRandomDigits(9)));
        lifeProfile.setEmail(faker.internet().emailAddress());
        lifeProfile.clickOnNextBtn();
        lifeProfile.clickOnOkGotItBtn();
        TestSuiteHelper.takeScreenShotHelper(extentTest);
        lifeProfile.clickOnInsurerDetails(insurer);
        String url = driver. getCurrentUrl();
        System.out.println("url is--->"+ url);
        TestSuiteHelper.takeScreenShotHelper(extentTest);
    }

    @Test(priority = 2)
    public void getRedirectionResult()  {
        extentTest = BaseTestHelper.addTestName(extentReport, extentTest);
        driver.get(Constant.BaseTestConstant.FLIPKARTURL);
        String url = "";
        flipKartHomePage = new FlipKartHomePage();
        flipKartHomePage.clickOnCloseBtn();
        flipKartHomePage.setSearchForProducts("books");
        flipKartHomePage.clickOnSearchBtn();
        List<WebElement> links = flipKartHomePage.getLinks();
        Iterator<WebElement> it = links.iterator();
        while(it.hasNext()) {
            url = it.next().getAttribute("href");
            if (url == null || url.isEmpty()) {
                System.out.println("URL is either not configured for anchor tag or it is empty");
                continue;
            }
            TestSuiteHelper.verifyLinks(url);
        }
        TestSuiteHelper.takeScreenShotHelper(extentTest);
    }

    @AfterMethod
    public void afterMethod() {
        if (extentReport != null) {
            extentReport.flush();
        }

    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        driver.quit();
    }


}
